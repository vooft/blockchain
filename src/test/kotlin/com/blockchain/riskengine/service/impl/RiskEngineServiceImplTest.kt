package com.blockchain.riskengine.service.impl

import com.blockchain.riskengine.account.*
import com.blockchain.riskengine.persistence.ConcurrentAccountModificationException
import com.blockchain.riskengine.service.WithdrawalRequest
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.*
import java.math.BigDecimal
import kotlin.test.assertFailsWith

private const val USER_ID = 100
private const val RETRIES = 20

internal class RiskEngineServiceImplTest {
    private lateinit var service: RiskEngineServiceImpl
    private lateinit var accountService: AccountService

    @BeforeEach
    fun setUp() {
        accountService = mock(AccountService::class.java)

        val account = PlainAccount(USER_ID, mapOf(Token.BTC to BigDecimal.TEN))
        doReturn(VersionedAccount(account, 1)).`when`(accountService).find(USER_ID)

        doThrow(ConcurrentAccountModificationException(account, 1, 1))
                .`when`(accountService).withdraw(anyObject(), anyLong(), anyObject())

        service = RiskEngineServiceImpl(accountService, RETRIES)
    }

    @Test
    fun testRetry() {
        val request = WithdrawalRequest(USER_ID, Amount(BigDecimal.ONE, Token.BTC))
        assertFailsWith(ConcurrentAccountModificationException::class) {
            service.withdrawIfPossible(request)
        }

        verify(accountService, times(RETRIES))
    }
}

private fun <T> anyObject(): T {
    Mockito.anyObject<T>()
    return uninitialized()
}

private fun <T> uninitialized(): T = null as T
