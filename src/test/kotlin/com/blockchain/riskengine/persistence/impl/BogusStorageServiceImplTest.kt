package com.blockchain.riskengine.persistence.impl

import com.blockchain.riskengine.account.PlainAccount
import com.blockchain.riskengine.account.Token
import com.blockchain.riskengine.account.VersionedAccount
import com.blockchain.riskengine.persistence.ConcurrentAccountModificationException
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import kotlin.test.assertFailsWith

private const val USER_ID = 100
private val BTC_BALANCE = BigDecimal.valueOf(99)
private val BTC_UPDATED = BigDecimal.valueOf(9)
private val USD_BALANCE = BigDecimal.valueOf(101)
private val USD_UPDATED = BigDecimal.valueOf(10)

internal class BogusStorageServiceImplTest {
    private lateinit var service: BogusStorageServiceImpl

    @BeforeEach
    fun setUp() {
        service = BogusStorageServiceImpl()
    }

    @Test
    fun testEmptyLookup() {
        assertNull(service.find(USER_ID))
    }

    @Test
    fun testAmounts() {
        val sourceAccount = PlainAccount(USER_ID, mapOf(Token.BTC to BTC_BALANCE, Token.USD to USD_BALANCE))
        service.save(VersionedAccount(sourceAccount, 1))

        verifyBalances(BTC_BALANCE, USD_BALANCE, 1)
    }

    @Test
    fun testUpdate() {
        val sourceAccount = PlainAccount(USER_ID, mapOf(Token.BTC to BTC_BALANCE, Token.USD to USD_BALANCE))
        service.save(VersionedAccount(sourceAccount, 1))

        val updatedAccount = PlainAccount(USER_ID, mapOf(Token.BTC to BTC_UPDATED, Token.USD to USD_UPDATED))
        service.save(VersionedAccount(updatedAccount, 2))

        verifyBalances(BTC_UPDATED, USD_UPDATED, 2)
    }

    @Test
    fun testConcurrentModificationException() {
        val sourceAccount = PlainAccount(USER_ID, mapOf(Token.BTC to BTC_BALANCE, Token.USD to USD_BALANCE))
        service.save(VersionedAccount(sourceAccount, 2))

        assertFailsWith(ConcurrentAccountModificationException::class) {
            service.save(VersionedAccount(sourceAccount, 1))
        }
    }

    private fun verifyBalances(btc: BigDecimal, usd: BigDecimal, version: Long) {
        val result = service.find(USER_ID)
        val account = result!!.account

        assertEquals(USER_ID, account.id)
        assertEquals(version, result.version)

        val balances = account.balances
        assertEquals(2, balances.size)

        assertEquals(btc, balances[Token.BTC])
        assertEquals(usd, balances[Token.USD])
    }
}
