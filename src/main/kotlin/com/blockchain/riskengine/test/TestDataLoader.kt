package com.blockchain.riskengine.test

import com.blockchain.riskengine.account.Account
import com.blockchain.riskengine.account.PlainAccount
import com.blockchain.riskengine.account.Token
import com.blockchain.riskengine.account.VersionedAccount
import com.blockchain.riskengine.persistence.StorageService
import com.fasterxml.jackson.dataformat.csv.CsvMapper
import com.fasterxml.jackson.dataformat.csv.CsvParser
import org.springframework.stereotype.Service
import org.springframework.util.ResourceUtils
import java.io.FileInputStream
import java.io.InputStream
import java.math.BigDecimal
import javax.annotation.PostConstruct

@Service
class TestDataLoader(private val service: StorageService) {
    @PostConstruct
    fun loadData() {
        val file = ResourceUtils.getFile("classpath:dataset.csv")
        val stream = FileInputStream(file)
        stream.use {  s ->
            // lazily read csv and save to db
            val accounts = parseAccounts(s)
            accounts.forEach { service.save(VersionedAccount(it, 1)) }
        }
    }

    fun parseAccounts(stream: InputStream): Sequence<Account> {
        val csvMapper = CsvMapper()
        csvMapper.enable(CsvParser.Feature.WRAP_AS_ARRAY)

        val reader = csvMapper.readerFor(Array<String>::class.java)

        val iterator = reader.readValues<Array<String>>(stream)
        return iterator.asSequence()
                .drop(1)
                .map { row ->
                    val id = row[0].toInt()
                    val balances = HashMap<Token, BigDecimal>()

                    for ((index, token) in Token.values().withIndex()) {
                        balances[token] = row[index + 1].toBigDecimal()
                    }

                    PlainAccount(id, balances.toMap())
                }
    }
}

