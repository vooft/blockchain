package com.blockchain.riskengine.persistence.impl

import com.blockchain.riskengine.account.VersionedAccount
import com.blockchain.riskengine.persistence.ConcurrentAccountModificationException
import com.blockchain.riskengine.persistence.StorageService
import org.springframework.stereotype.Service
import java.util.concurrent.ConcurrentHashMap

/**
 * Simple in-memory storage with concurrency support
 */
@Service
class BogusStorageServiceImpl : StorageService {
    private val map = ConcurrentHashMap<Int, VersionedAccount>()

    override fun find(id: Int): VersionedAccount? {
        return map[id]
    }

    override fun save(newVersion: VersionedAccount) {
        // synchronize on account level, still need some locking system
        map.compute(newVersion.account.id) { _, existing ->
            val account = newVersion.account
            if (existing != null && existing.version >= newVersion.version) {
                throw ConcurrentAccountModificationException(account, newVersion.version, existing.version)
            }

            newVersion
        }
    }
}
