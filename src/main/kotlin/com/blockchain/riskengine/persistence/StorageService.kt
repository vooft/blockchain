package com.blockchain.riskengine.persistence

import com.blockchain.riskengine.account.Account
import com.blockchain.riskengine.account.VersionedAccount

/**
 * Transactional storage service
 */
interface StorageService {
    /**
     * Performs a lookup for a given user
     */
    fun find(id: Int): VersionedAccount?

    /**
     * Checks the account version and throws ConcurrentAccountModificationException if it is incorrect
     */
    fun save(newVersion: VersionedAccount)
}

data class ConcurrentAccountModificationException(val account: Account, val expected: Long, val seen: Long)
    : RuntimeException("Account ${account.id} version mismatch. Expected: $expected, actual: $seen")
