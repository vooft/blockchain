package com.blockchain.riskengine.account

import java.math.BigDecimal

enum class Token {
    USD,
    EUR,
    BTC,
    BCH,
    ETH
}

data class Amount(val amount: BigDecimal, val token: Token)
data class SettledAmount(val bought: Amount, val sold: Amount)

sealed class Account {
    abstract val id: Int
    abstract val balances: Map<Token, BigDecimal>
}

/**
 * Basic account class that doesn't have a running transaction
 */
data class PlainAccount(override val id: Int,
                        override val balances: Map<Token, BigDecimal>) : Account()

/**
 * Account class with running transaction
 */
data class AccountWithRequestedAmount(override val id: Int,
                                      override val balances: Map<Token, BigDecimal>,
                                      val requestedAmount: Amount) : Account()

/**
 * Account wrapper for optimistic locking
 */
data class VersionedAccount(val account: Account, val version: Long)


