package com.blockchain.riskengine.account.impl

import com.blockchain.riskengine.account.*
import com.blockchain.riskengine.persistence.StorageService
import com.github.benmanes.caffeine.cache.Caffeine
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service
class CachedAccountServiceImpl(private val storageService: StorageService) : AccountService {
    // caffeine uses LFU strategy
    private val cache = Caffeine.newBuilder()
            .maximumSize(300)
            .build<Int, Any>()

    private val nullMarker = Object()

    override fun find(id: Int): VersionedAccount {
        val result = cache.get(id) {
            // non-existing account is perfectly valid, store "miss" too
            storageService.find(it) ?: nullMarker
        }

        if (result == nullMarker) {
            throw AccountNotFoundException(id)
        } else {
            return result as VersionedAccount
        }
    }

    override fun settle(account: Account, seenVersion: Long, settled: SettledAmount) {
        val balances = account.balances.toMutableMap()

        val oldBought = balances[settled.bought.token] ?: BigDecimal.ZERO
        val oldSold = balances[settled.sold.token]!!

        balances[settled.bought.token] = oldBought.plus(settled.bought.amount)
        balances[settled.sold.token] = oldSold.minus(settled.sold.amount)

        val newAccount = PlainAccount(account.id, balances.toMap())
        storageService.save(VersionedAccount(newAccount, seenVersion + 1))

        // we can't update db and refresh cache atomically, safer just to invalidate
        cache.invalidate(account.id)
    }

    override fun withdraw(account: Account, seenVersion: Long, requested: Amount) {
        val balances = account.balances.toMutableMap()
        val oldAmount = balances[requested.token]!!

        balances[requested.token] = oldAmount.minus(requested.amount)

        val newAccount = AccountWithRequestedAmount(account.id, balances.toMap(), requested)
        storageService.save(VersionedAccount(newAccount, seenVersion + 1))

        // we can't update db and refresh cache atomically, safer just to invalidate
        // depending on throughput it should be possible to store list of running withdrawals separately
        cache.invalidate(account.id)
    }
}
