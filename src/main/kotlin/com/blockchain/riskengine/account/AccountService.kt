package com.blockchain.riskengine.account

/**
 * Service that performs basic account operations
 * Doesn't perform any checks
 */
interface AccountService {
    fun find(id: Int): VersionedAccount
    fun settle(account: Account, seenVersion: Long, settled: SettledAmount)
    fun withdraw(account: Account, seenVersion: Long, requested: Amount)
}

class AccountNotFoundException(id: Int) : RuntimeException("Account $id not found")
