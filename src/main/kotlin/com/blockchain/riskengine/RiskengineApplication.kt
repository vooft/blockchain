package com.blockchain.riskengine

import com.blockchain.riskengine.account.Amount
import com.blockchain.riskengine.account.Token
import com.blockchain.riskengine.service.RiskEngineService
import com.blockchain.riskengine.service.WithdrawalRequest
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal

@SpringBootApplication
class RiskengineApplication

@RestController
class WithdrawBalanceController(private val withdrawalService: RiskEngineService) {
	@GetMapping("/check")
	fun check(@RequestParam accountId: Int, @RequestParam token: Token, @RequestParam amount: BigDecimal): String {
		val request = WithdrawalRequest(accountId, Amount(amount, token))
		return if (withdrawalService.withdrawIfPossible(request)) {
			"SUFFICIENT_BALANCE"
		} else {
			"INSUFFICIENT_BALANCE"
		}
	}
}

fun main(args: Array<String>) {
	runApplication<RiskengineApplication>(*args)
}
