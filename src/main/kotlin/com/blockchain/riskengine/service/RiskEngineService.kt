package com.blockchain.riskengine.service

import com.blockchain.riskengine.account.Amount
import com.blockchain.riskengine.account.SettledAmount

/**
 * Main business-logic class, performs all checks, retries if neccessary, etc
 */
interface RiskEngineService {
    fun settle(request: SettlementRequest)
    fun withdrawIfPossible(request: WithdrawalRequest): Boolean
}

data class SettlementRequest(val accountId: Int, val settledAmount: SettledAmount)
data class WithdrawalRequest(val accountId: Int, val requestedAmount: Amount)

data class TransactionNotFoundException(val accountId: Int)
    : RuntimeException("Transaction is not running for account $accountId")
data class TransactionAlreadyRunningException(val accountId: Int)
    : RuntimeException("Transaction is already running for account $accountId")
