package com.blockchain.riskengine.service.impl

import com.blockchain.riskengine.account.AccountService
import com.blockchain.riskengine.account.AccountWithRequestedAmount
import com.blockchain.riskengine.persistence.ConcurrentAccountModificationException
import com.blockchain.riskengine.service.*
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class RiskEngineServiceImpl(private val accountService: AccountService,
                            @Value("riskengine.concurrency.retries") private val retries: Int) : RiskEngineService {

    override fun settle(request: SettlementRequest) {
        return retryOnConcurrentModificationException {
            val versionedAccount = accountService.find(request.accountId)
            val account = versionedAccount.account

            if (account !is AccountWithRequestedAmount) {
                throw TransactionNotFoundException(account.id)
            }

            accountService.settle(account, versionedAccount.version, request.settledAmount)
        }
    }

    override fun withdrawIfPossible(request: WithdrawalRequest): Boolean {
        return retryOnConcurrentModificationException {
            val versionedAccount = accountService.find(request.accountId)
            val account = versionedAccount.account

            if (account is AccountWithRequestedAmount) {
                throw TransactionAlreadyRunningException(account.id)
            }

            val currentAmount = account.balances.getValue(request.requestedAmount.token)

            if (currentAmount > request.requestedAmount.amount) {
                accountService.withdraw(account, versionedAccount.version, request.requestedAmount)
                true
            } else {
                false
            }
        }
    }

    private fun <T> retryOnConcurrentModificationException(method: () -> T): T {
        // try retries - 1 times
        for (i in 0 until retries - 1) {
            try {
                return method()
            } catch (e: ConcurrentAccountModificationException) {
                // ignore
            }
        }

        // throw any other ConcurrentAccountModificationException
        return method()
    }
}
