package com.blockchain.riskengine.kafka

import com.blockchain.riskengine.account.Amount
import com.blockchain.riskengine.account.SettledAmount
import com.blockchain.riskengine.account.Token
import com.blockchain.riskengine.service.RiskEngineService
import com.blockchain.riskengine.service.SettlementRequest
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Service
import java.math.BigDecimal

/**
 * Simple kafka listener
 */
@Service
class SettlementMessageListener(private val riskEngineService: RiskEngineService) {
    private val mapper = jacksonObjectMapper()

    @KafkaListener(topics = ["settlement"])
    fun consume(string: String) { // jackson deserializer doesn't work out of box, will require some research
        val message = mapper.readValue(string, JsonSettlement::class.java)

        val bought = Amount(message.boughtQuantity!!, message.boughtToken!!)
        val sold = Amount(message.soldQuantity!!, message.soldToken!!)
        riskEngineService.settle(SettlementRequest(message.userId, SettledAmount(bought, sold)))
    }
}

class JsonSettlement {
    @JsonProperty("user_id")
    var userId: Int = -1

    @JsonProperty("bought_token")
    var boughtToken: Token? = null

    @JsonProperty("bought_quantity")
    var boughtQuantity: BigDecimal? = null

    @JsonProperty("sold_token")
    var soldToken: Token? = null

    @JsonProperty("sold_quantity")
    var soldQuantity: BigDecimal? = null
}
